package com.bstek.urule.debug;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Jacky.gao
 * @since 2017年11月27日
 */
public class DefaultHtmlFileDebugWriter implements DebugWriter {
    private String path;

    @Override
    public void write(List<MessageItem> items) throws IOException {
        if (StringUtils.isBlank(path)) {
            return;
        }
        StringBuilder msg = new StringBuilder();
        for (MessageItem item : items) {
            msg.append(item.toHtml());
        }
        String fullPath = path + "/urule-debug.html";
        String sb = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>URule调试日志信息</title><body style='font-size:12px'>" +
                msg.toString() +
                "</body></html>";
        FileOutputStream out = new FileOutputStream(fullPath);
        IOUtils.write(sb, out, StandardCharsets.UTF_8);
        out.flush();
        out.close();
    }

    public void setPath(String path) {
        this.path = path;
    }
}
