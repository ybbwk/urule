/*******************************************************************************
 * Copyright 2017 Bstek
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.urule.parse;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.bstek.urule.model.library.variable.Variable;

/**
 * @author Jacky.gao
 * @since 2015年3月10日
 */
public class ParameterLibraryParser implements Parser<List<Variable>> {
    private VariableParser variableParser;

    @Override
    public List<Variable> parse(Element element) {
        List<Variable> variables = new ArrayList<>();
        for (Object obj : element.elements()) {
            if (!(obj instanceof Element)) {
                continue;
            }
            Element ele = (Element) obj;
            String name = ele.getName();
            if ("parameter".equals(name)) {
                variables.add(variableParser.parse(ele));
            }
        }
        return variables;
    }

    @Override
    public boolean support(String name) {
        return "parameter-library".equals(name);
    }

    public void setVariableParser(VariableParser variableParser) {
        this.variableParser = variableParser;
    }
}
